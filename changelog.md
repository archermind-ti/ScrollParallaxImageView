## V1.0.0-beta

1. 移植完成， 目前支持原库所有功能，显示效果基本一致。
原库差异：
1. 由于ListContainer中所有Item中的控件调用getLocationOnScreen，得到的结果都是一样的。导致初始化时存在显示差异。
2. Canvas中的translate,scale接口显示效果与原库不一致， 目前显示效果略有差异。
3. 由于ListContainer中的子控件使用getComponentTreeObserver，无法监听滚动事件，目前增加接口setComponentTreeObserver将父控件获取的ComponentTreeObserver传给组件使用。