package com.gjiazhe.scrollparallaximageview.parallaxstyle;

import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;

/**
 * When the imageView is scrolling horizontally, the image in imageView will change its alpha.
 * The alpha is according to the horizontal position of the imageView and range
 * from 1.0f to <code>finalAlpha</code>.
 * When the imageView is at the middle of the screen, the alpha is 1.0f. And When
 * it just scroll out of the screen, the alpha is <code>finalAlpha</code>.
 *
 * Created by gjz on 26/11/2016.
 */

public class HorizontalAlphaStyle implements ScrollParallaxImageView.ParallaxStyle {
    private float finalAlpha = 0.1f;
    private final RectFloat rect = new RectFloat();
    public HorizontalAlphaStyle() {}

    public HorizontalAlphaStyle(float finalAlpha) {
        if (finalAlpha < 0 || finalAlpha > 1.0f) {
            throw new IllegalArgumentException("the alpha must between 0 and 1.");
        }
        this.finalAlpha = finalAlpha;
    }

    public void setFinalAlpha(float alpha) {
        finalAlpha = alpha;
    }

    @Override
    public void transform(ScrollParallaxImageView view, Canvas canvas, int x, int y) {
        // view's width
        int vWidth = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        int vHeight = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        // device's width
        int dWidth = view.getContext().getResourceManager().getDeviceCapability().width
                * view.getContext().getResourceManager().getDeviceCapability().screenDensity
                / 160;

        if (vWidth >= dWidth) {
            // Do nothing if imageView's width is bigger than device's width.
            return;
        }

        rect.left = 0;
        rect.right = vWidth;
        rect.top = 0;
        rect.bottom = vHeight;

        float alpha;
        int pivot = (dWidth - vWidth) / 2;
        if (x <= pivot) {
            alpha = 2 * (1 - finalAlpha) * (x + vWidth) / (dWidth + vWidth) + finalAlpha;
        } else {
            alpha = 2 * (1 - finalAlpha) * (dWidth - x) / (dWidth + vWidth) + finalAlpha;
        }
        canvas.save();
        Paint paint = new Paint();
        paint.setAlpha(alpha);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(view.getOriginPixelMap()), rect, paint);
        canvas.restore();
    }

    @Override
    public void onAttachedToImageView(ScrollParallaxImageView view) {

    }

    @Override
    public void onDetachedFromImageView(ScrollParallaxImageView view) {

    }
}
