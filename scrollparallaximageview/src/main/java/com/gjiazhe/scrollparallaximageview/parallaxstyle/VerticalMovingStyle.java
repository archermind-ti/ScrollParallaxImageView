package com.gjiazhe.scrollparallaximageview.parallaxstyle;

import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;

/**
 * When the imageView is scrolling vertically, the image in imageView will be
 * also scrolling vertically if the image' height is bigger than imageView's height.
 *
 * The image will not over scroll to it's view bounds.
 *
 * Note: it only supports imageView with CENTER_CROP scale type.
 *
 * Created by gjz on 25/11/2016.
 */

public class VerticalMovingStyle implements ScrollParallaxImageView.ParallaxStyle {
    private final RectFloat rect = new RectFloat();
    private boolean runFirst = true;

    @Override
    public void onAttachedToImageView(ScrollParallaxImageView view) {
        // only supports CENTER_CROP
        view.setScaleMode(Image.ScaleMode.CLIP_CENTER);
    }

    @Override
    public void onDetachedFromImageView(ScrollParallaxImageView view) {

    }

    @Override
    public void transform(ScrollParallaxImageView view, Canvas canvas, int x, int y) {
        if (view.getScaleMode() != Image.ScaleMode.CLIP_CENTER) {
            return;
        }

        // image's width and height
        int iWidth = view.getOriginPixelMap().getImageInfo().size.width;
        int iHeight = view.getOriginPixelMap().getImageInfo().size.height;
        if (iWidth <= 0 || iHeight <= 0) {
            return;
        }

        // view's width and height
        int vWidth = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        int vHeight = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();

        rect.left = 0;
        rect.right = vWidth;
        rect.top = 0;
        rect.bottom = vHeight;

        // device's height
        int dHeight = view.getContext().getResourceManager().getDeviceCapability().height
                * view.getContext().getResourceManager().getDeviceCapability().screenDensity
                / 160;

        if (iWidth * vHeight < iHeight * vWidth) {
            // avoid over scroll
            if (y < -vHeight) {
                y = -vHeight;
            } else if (y > dHeight) {
                y = dHeight;
            }

            float imgScale = (float) vWidth / (float) iWidth;
            float max_dy = Math.abs((iHeight * imgScale - vHeight) * 0.5f);
            float translateY = -(2 * max_dy * y + max_dy * (vHeight - dHeight)) / (vHeight + dHeight);
            if (!runFirst) {
                canvas.translate(0, translateY);
            }
            runFirst = false;
            canvas.drawPixelMapHolderRect(new PixelMapHolder(view.getOriginPixelMap()), rect,new Paint());
            canvas.restore();
        }
    }
}
