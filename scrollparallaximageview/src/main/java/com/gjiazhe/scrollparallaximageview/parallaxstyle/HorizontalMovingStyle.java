package com.gjiazhe.scrollparallaximageview.parallaxstyle;

import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;

/**
 * When the imageView is scrolling horizontally, the image in imageView will be
 * also scrolling horizontally if the image' width is bigger than imageView's width.
 *
 * The image will not over scroll to it's view bounds.
 *
 * Note: it only supports imageView with CENTER_CROP scale type.
 *
 * Created by gjz on 26/11/2016.
 */

public class HorizontalMovingStyle implements ScrollParallaxImageView.ParallaxStyle {
    private final RectFloat rect = new RectFloat();
    private boolean runFirst = true;
    @Override
    public void onAttachedToImageView(ScrollParallaxImageView view) {
        // only supports CENTER_CROP
        view.setScaleMode(Image.ScaleMode.CLIP_CENTER);
    }

    @Override
    public void onDetachedFromImageView(ScrollParallaxImageView view) {

    }

    @Override
    public void transform(ScrollParallaxImageView view, Canvas canvas, int x, int y) {
        if (view.getScaleMode() != Image.ScaleMode.CLIP_CENTER) {
            return;
        }

        // image's width and height
        int iWidth = view.getOriginPixelMap().getImageInfo().size.width;
        int iHeight = view.getOriginPixelMap().getImageInfo().size.height;
        if (iWidth <= 0 || iHeight <= 0) {
            return;
        }

        // view's width and height
        int vWidth = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        int vHeight = view.getHeight() - view.getPaddingTop() - view.getPaddingBottom();
        rect.left = 0;
        rect.right = vWidth;
        rect.top = 0;
        rect.bottom = vHeight;

        // device's width
        int dWidth = view.getContext().getResourceManager().getDeviceCapability().width
                * view.getContext().getResourceManager().getDeviceCapability().screenDensity
                / 160;
        int count = canvas.getSaveCount();
        if (iWidth * vHeight > iHeight * vWidth) {
            // avoid over scroll
            if (x < -vWidth) {
                x = -vWidth;
            } else if (x > dWidth) {
                x = dWidth;
            }

            float imgScale = (float) vHeight / (float) iHeight;
            float max_dx = Math.abs((iWidth * imgScale - vWidth) * 0.5f);
            float translateX = -(2 * max_dx * x + max_dx * (vWidth - dWidth)) / (vWidth + dWidth);
            if (!runFirst) {
                canvas.translate(translateX, 0);
            }
            runFirst = false;
            canvas.drawPixelMapHolderRect(new PixelMapHolder(view.getOriginPixelMap()), rect,new Paint());
            canvas.restoreToCount(count);
        }


    }
}
