package com.gjiazhe.scrollparallaximageview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * Created by gjz on 25/11/2016.
 */

public class ScrollParallaxImageView extends Image
        implements ComponentTreeObserver.ScrollChangedListener, Component.EstimateSizeListener, Component.DrawTask {
    private int[] viewLocation = new int[2];
    private boolean enableScrollParallax = true;
    private ParallaxStyle parallaxStyles;
    private ComponentTreeObserver componentTreeObserver;

    public PixelMap getOriginPixelMap() {
        return originPixelMap;
    }

    public void setOriginPixelMap(PixelMap originPixelMap) {
        this.originPixelMap = originPixelMap;
        invalidate();
    }

    private PixelMap originPixelMap;

    public ScrollParallaxImageView(Context context) {
        this(context, null);
    }

    public ScrollParallaxImageView(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public ScrollParallaxImageView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);

        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    public void setComponentTreeObserver(ComponentTreeObserver componentTreeObserver ) {
        if (componentTreeObserver != null) {
            this.componentTreeObserver = componentTreeObserver;
            setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                componentTreeObserver.addScrolledListener(ScrollParallaxImageView.this);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                componentTreeObserver.removeScrolledListener(ScrollParallaxImageView.this);
            }
        });
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (!enableScrollParallax || getOriginPixelMap() == null) {
            return;
        }

        if (parallaxStyles != null){
            viewLocation = this.getLocationOnScreen();
            parallaxStyles.transform(this, canvas, viewLocation[0], viewLocation[1]);
        }
    }

    @Override
    public void onScrolled() {
        if (enableScrollParallax) {
            invalidate();
        }
    }


    public void setParallaxStyles(ParallaxStyle styles) {
        if (parallaxStyles != null) {
            parallaxStyles.onDetachedFromImageView(this);
        }
        parallaxStyles = styles;
        parallaxStyles.onAttachedToImageView(this);
    }

    public void setEnableScrollParallax(boolean enableScrollParallax) {
        this.enableScrollParallax = enableScrollParallax;
    }




    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(Component.EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, EstimateSpec.NOT_EXCEED));
        return false;
    }

    public interface ParallaxStyle {
        void onAttachedToImageView(ScrollParallaxImageView view);
        void onDetachedFromImageView(ScrollParallaxImageView view);
        void transform(ScrollParallaxImageView view, Canvas canvas, int x, int y);
    }
}
