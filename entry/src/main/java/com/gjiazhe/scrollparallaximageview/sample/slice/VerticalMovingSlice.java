/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gjiazhe.scrollparallaximageview.sample.slice;

import com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView;
import com.gjiazhe.scrollparallaximageview.parallaxstyle.VerticalMovingStyle;
import com.gjiazhe.scrollparallaximageview.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class VerticalMovingSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_vertital_main);
        ListContainer rvVertical = (ListContainer) findComponentById(ResourceTable.Id_rv_vertical);
        LayoutManager layoutManager1 = new DirectionalLayoutManager();
        layoutManager1.setOrientation(Component.VERTICAL);
        rvVertical.setLayoutManager(layoutManager1);
        rvVertical.canScroll(Component.VERTICAL);
        rvVertical.setLongClickable(false);
        rvVertical.setItemProvider(new MyItemProvider(new VerticalMovingStyle(), rvVertical));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private static class ImageItem {
        private int mId;

        public ImageItem(int id) {
            mId = id;
        }

        public int getmId() {
            return mId;
        }
    }

    private class MyItemProvider extends BaseItemProvider {
        private List<ImageItem> items;
        private ScrollParallaxImageView.ParallaxStyle parallaxStyle;
        private ComponentContainer parent;

        MyItemProvider(ScrollParallaxImageView.ParallaxStyle parallaxStyle, ComponentContainer parent) {
            this.parent = parent;
            this.parallaxStyle = parallaxStyle;
            items = new ArrayList<>();
            for (int i = 0; i < 15; i++) {
                switch (i % 5) {
                    case 0:
                        items.add(new ImageItem(ResourceTable.Media_pic1));
                        break;
                    case 1:
                        items.add(new ImageItem(ResourceTable.Media_pic2));
                        break;
                    case 2:
                        items.add(new ImageItem(ResourceTable.Media_pic3));
                        break;
                    case 3:
                        items.add(new ImageItem(ResourceTable.Media_pic4));
                        break;
                    case 4:
                        items.add(new ImageItem(ResourceTable.Media_pic5));
                        break;
                }
            }
        }

        @Override
        public int getCount() {
            return 15;
        }

        @Override
        public Object getItem(int i) {
            if (items != null && i >= 0 && i < items.size()) {
                return items.get(i);
            }
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            final Component cpt;
            if (component == null) {
                cpt = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_item_img_vertical, null, false);
                ImageItem item = items.get(i);
                ScrollParallaxImageView image = (ScrollParallaxImageView) cpt.findComponentById(ResourceTable.Id_img);
                image.setComponentTreeObserver(parent.getComponentTreeObserver());
                image.setParallaxStyles(parallaxStyle);
                InputStream inputStream = null;
                try {
                    inputStream = getResourceManager().getResource(item.getmId());
                    ImageSource source = ImageSource.create(inputStream, null);
                    image.setOriginPixelMap(source.createPixelmap(null));
                } catch (IOException | NotExistException e) {
                    e.printStackTrace();
                }
            } else {
                cpt = component;
            }
            return cpt;
        }
    }
}
