# ScrollParallaxImageView

ScrollParallaxImageView扩展与Image, 当滚动的时候提供视差效果. 它可以用于任何可以滚动内容的容器，如ListContainer, ScrollView等等.

## 演示效果
![](img/horizontal.gif)
![](img/verticalalpha.gif)
![](img/verticalmoving.gif)
![](img/verticalscale.gif)



## 集成

With gradle:

```groovy
dependencies {
   compile 'com.gitee.archermind-ti:scrollparallaximageview:1.0.0-beta'
}
```

## 使用方式

#### 布局文件

```xml
<com.gjiazhe.scrollparallaximageview.ScrollParallaxImageView
	ohos:id="$+id:img"
	ohos:height="200vp"
	ohos:width="200vp"
	ohos:scale_mode="clip_center"
	/>
```

#### 代码设置

```java
ScrollParallaxImageView iv = (ScrollParallaxImageView)findComponentById(ResourceTable.Id_img);
iv.setParallaxStyles(new VerticalMovingStyle()); // or other parallax styles
```

目前已实现HorizontalAlphaStyle，HorizontalMovingStyle，HorizontalScaleStyle，VerticalAlphaStyle，VerticalMovingStyle，VerticalScaleStyle六种视差效果。

也可以通过实现接口`ScrollParallaxImageView.ParallaxStyle`来实现自己的效果 :

```java
public class MyParallaxStyle implements ScrollParallaxImageView.ParallaxStyle {
  @Override
    public void transform(ScrollParallaxImageView view, Canvas canvas, int x, int y) {
        // Transform the view here, x and y stands for the coordinate of
        // ScrollParallaxImageView's top-left corner.
    }
  
    @Override
    public void onAttachedToImageView(ScrollParallaxImageView view) {
		// Do something when the style is set to a ScrollParallaxImageView.
    }

    @Override
    public void onDetachedFromImageView(ScrollParallaxImageView view) {
		// Do something when the style is remove from a ScrollParallaxImageView.
    }
}
```

## 遗留问题

1. 由于ListContainer中所有Item中的控件调用getLocationOnScreen，得到的结果都是一样的。导致初始化时显示略有差异。
2. Canvas中的translate,scale接口显示效果与原库不一致， 目前显示效果略有差异。

## License

    MIT License
    
    Copyright (c) 2016 郭佳哲
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

